package com.example.servicec;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ServiceCApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceCApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ServiceCApplication.class, args);
    }

    @GetMapping("/foo/info")
    public Map<String, String> info(HttpServletRequest request) {
        LOGGER.info("Menerima request di {} : {}", request.getLocalAddr(),request.getLocalPort());
        Map<String, String> hasil = new HashMap<>();
        hasil.put("waktu", LocalDateTime.now().toString());
        hasil.put("host", request.getLocalName());
        return hasil;
    }
}
